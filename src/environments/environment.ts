export const environment = {
  production: false,
  url_api: 'http://localhost:8000/api',
  url_economy: 'https://www.indecon.online'
};
