import { TestBed } from '@angular/core/testing';

import { EconomyService } from './economy.service';

describe('EconomyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EconomyService = TestBed.get(EconomyService);
    expect(service).toBeTruthy();
  });
});
