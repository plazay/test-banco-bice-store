import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EconomyService {

  constructor(
    private http: HttpClient
  ) { }

  getAllPrice() {
    return this.http.get(`${environment.url_api}/currencies`);
    // return this.http.get(`${environment.url_economy}/last`);
  }
}
