import { Component, OnInit } from '@angular/core';
import { EconomyService } from './../../core/services/economy.service';

@Component({
  selector: 'app-economy',
  templateUrl: './economy.component.html',
  styleUrls: ['./economy.component.scss']
})
export class EconomyComponent implements OnInit {

  economy;

  constructor(
    private economyService: EconomyService
  ) { }

  ngOnInit() {
    this.fetchEconomy();
  }

  fetchEconomy() {
    this.economyService.getAllPrice()
    .subscribe(economy => {
      this.economy = economy;
    });
  }
}
