import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EconomyRoutingModule } from './economy-routing.module';
import { EconomyComponent } from './components/economy.component';

import { MaterialModule } from './../material/material.module';
import { SharedModule } from './../shared/shared.module';

@NgModule({
  declarations: [EconomyComponent],
  imports: [
    CommonModule,
    EconomyRoutingModule,
    SharedModule,
    MaterialModule
  ]
})
export class EconomyModule { }
