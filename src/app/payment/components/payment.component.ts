import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Product } from './../../core/models/product.model';
import { CartService } from './../../core/services/cart.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {


  products: Observable<Product[]>;

  constructor(
    private cartService: CartService
  ) {
    console.log(this.products);
    this.products = this.cartService.cart$;
   }

  ngOnInit() {
  }

}
